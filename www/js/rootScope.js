angular.module('starter')

.run(function ($rootScope, $state, $location, $ionicHistory, $http, $timeout, $ionicLoading, $cordovaDevice, $ionicPlatform, $cordovaToast, $ionicModal) {

	
	$rootScope.$on('$stateChangeStart', function (event, toState, toParams){
		event.defaultPrevented = false;
		$ionicLoading.show();
		//utils.gaTrackView(location.hash.replace('#/app', ''));

		if(typeof $rootScope.loginSession == 'undefined'){
			// get me a login modal!

			if(localStorage.getItem('login') == null){
				
				try{ 
					var uuid = $cordovaDevice.getUUID(); 
				}catch(e){ 
					var uuid = 'development-uuid';
				}

			}else{
				$rootScope.loginSession = JSON.parse(localStorage.getItem('login'))
				$state.go(toState.name, toParams);
			}

		}

		if(localStorage.getItem('token_webservice') != null){
			$http.defaults.headers.common.Authorization = 'Token ' + JSON.parse(localStorage.getItem('token_webservice'));
		}

		return false;
	});

	$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams){
		$timeout(function(){
			$ionicLoading.hide();	
		}, 400)
	});

	setInterval(function(){
		if(localStorage.getItem('login') != null) $rootScope.loginSession = JSON.parse(localStorage.getItem('login'));
		$rootScope.$applyAsync();
	}, 1000);
	
	$http
		.get(window.SERVER + '/api/settings')
		.success(function(data){
			$rootScope.settings = data.results[0];
		});

	$rootScope.calendar = function(uuid,title,location,startDate,endDate){

		var status = localStorage.getItem('calendar-'+ uuid);
		var startDate = moment(startDate).toDate();
		var endDate = moment(endDate).toDate();
		var notes = 'Agregado por app Bookafé Maipú';
		if(status == null){
			
			window.plugins.calendar.createEvent(title,location,notes,startDate,endDate,function(){
				localStorage.setItem('calendar-'+ uuid, true);
				$cordovaToast.showShortBottom('Evento guardado con éxito');
				$rootScope.$apply();
			}, function (err) {
				navigator.notification.alert('No fue posible generar evento:'+ JSON.stringify(err));
			});
		}
		if(status != null){
			
			window.plugins.calendar.deleteEvent(title,location,notes,startDate,endDate,function(result){
				localStorage.removeItem('calendar-'+ match.uuid)
				$cordovaToast.showShortBottom('Evento eliminado con éxito');
				$rootScope.$apply();
			}, function(err){

			});
		}
	}
	$rootScope.check_event = function(uuid){
		var status = localStorage.getItem('calendar-'+ uuid);
		if(status == null){
			return "Agendar";
		}else{
			return "No agendar";
		}
	}
	$rootScope.fullImage = function(imageSrc){

		$rootScope.fullscreen_image = imageSrc;

		$ionicModal.fromTemplateUrl('templates/image-modal.html', {
	    	scope: $rootScope,
	    	animation: 'slide-in-up'
	    }).then(function(modal) {
	    	$rootScope.modal_image = modal;
	    	$rootScope.modal_image.show()
	    });
	}
	$rootScope.closeFullscreenImage = function(){
		$rootScope.modal_image.remove();
	}
});