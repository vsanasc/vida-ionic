angular.module('starter.filters')

.filter('htmlparagraph', function(){
	return function(input){

		if(/<[a-z][\s\S]*>/i.test(input)) return input;

		var reg = new RegExp('\n', 'g');
		return '<p>' + (input + ' ').replace(reg, '</p><p>') + '</p>';
	}
})