angular.module('starter.controllers')

.controller('DetailReflexionCtrl', function($scope, $stateParams, $timeout, ionicMaterialInk, ionicMaterialMotion, ReflexionREST, $cordovaToast, $ionicLoading) {

	$ionicLoading.show();

	ReflexionREST.get({slug:$stateParams.slug}, function(data){
		$scope.reflexion = data;

		$ionicLoading.hide();
	}, function(err){
		
	});
    // Set Ink
    ionicMaterialInk.displayEffect();

})