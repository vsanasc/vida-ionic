angular.module('starter.controllers')

.controller('SettingsCtrl', function($scope, $timeout, $stateParams, ionicMaterialInk, ionicMaterialMotion) {

    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);

      // Set Ink
    ionicMaterialInk.displayEffect();

})