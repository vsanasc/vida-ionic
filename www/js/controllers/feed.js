angular.module('starter.controllers')

.controller('FeedCtrl', function($scope, $timeout, FeedREST, $stateParams, ionicMaterialInk, ionicMaterialMotion, $ionicLoading) {
  	
  	$ionicLoading.show();

	FeedREST.list(function(data){
		$scope.feed = data.results;
		$ionicLoading.hide();
	});

    ionicMaterialInk.displayEffect();
})