angular.module('starter.controllers')

.controller('LoginCtrl', function($scope, $rootScope, $timeout, $stateParams, $state, ionicMaterialInk, $http, $cordovaToast, $cordovaDevice, $ionicHistory, $ionicPlatform) {
  	
  	$scope.facebook = function(){

		//facebookConnectPlugin.logout();

		facebookConnectPlugin.login(['public_profile', 'email'], function(data){
			alert(JSON.stringify(data))
			facebookConnectPlugin.api(data.authResponse.userID+ '/?fields=id,email,first_name,last_name,link', ['public_profile', 'email'], function(response){
							
				var params = {
								platform: ionic.Platform.platform(),
								device: window.UUID_DEVICE,
								token_device: localStorage.getItem('token_device'),
								first_name:response.first_name,
								last_name:response.last_name,
								email:response.email,
								id_facebook: response.id
				};	

				$http
					.post(window.SERVER + '/api/login-facebook/', params)
					.success(function(data_server, status, headers, config){
						
						if(data_server.success){
							localStorage.setItem('login', JSON.stringify(data_server.data));

							$ionicHistory.nextViewOptions({disableBack: true});
							$state.go('app.feed');
						}
					})
					.error(function(data_server, status, headers, config){
						$cordovaToast.showShortBottom('Error al comunicarse con el servidor, intente más tarde!');
					});


			}, function(err){
				$cordovaToast.showShortBottom(err);
			})


		}, function(err){
			if(err.indexOf("Permission denied") !== -1 )
				$cordovaToast.showShortBottom("Debe autorizar la aplicación para poder hacer login");
			else
				$cordovaToast.showShortBottom(err);
		});

	}
	$scope.anonymus = function(){
	
		$http
			.post(window.SERVER + '/api/login-anonymus/', {platform:ionic.Platform.platform(), uuid:window.UUID_DEVICE, token_device:localStorage.getItem('token_device')})
			.success(function(data){ 
				var datauser = {id:data.id, token_webservice: data.token_webservice, points: data.points, kind:data.kind};
				$rootScope.loginSession = datauser;
				localStorage.setItem('login', JSON.stringify(datauser))
				localStorage.setItem('token_webservice', JSON.stringify(data.token_webservice));

				$ionicHistory.nextViewOptions({disableBack: true});
				$state.go('app.feed');
			})
			.error(function(err){
				$cordovaToast.showShortBottom('Error al comunicarse con el servidor, intente más tarde!');
			})
	}
    ionicMaterialInk.displayEffect();
})