angular.module('starter.controllers')

.controller('AlertCtrl', function($scope, $timeout, $stateParams, ionicMaterialInk, ionicMaterialMotion, AlertREST, $ionicLoading, $ionicLoading) {

	$ionicLoading.show();
	
	AlertREST.list(function(data){
		
		$scope.alerts = data.results;
		
		$ionicLoading.hide();
		
	  	$timeout(function() {
	        ionicMaterialMotion.fadeSlideIn({
	            selector: '.animate-fade-slide-in .item'
	        });
	    }, 200);
	});

    

    ionicMaterialInk.displayEffect();
})