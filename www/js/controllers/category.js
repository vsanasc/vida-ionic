angular.module('starter.controllers')

.controller('CategoryCtrl', function($scope, $timeout, ionicMaterialInk, ionicMaterialMotion, CategoryREST, $ionicLoading) {
	
	$ionicLoading.show();
	
	CategoryREST.list(function(data){
		$scope.categories = data.results;
		$ionicLoading.hide();
	})

    // Set Ink
    ionicMaterialInk.displayEffect();

})