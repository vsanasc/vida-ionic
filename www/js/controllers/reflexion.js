angular.module('starter.controllers')

.controller('ReflexionCtrl', function($scope, $stateParams, $timeout, ionicMaterialInk, ionicMaterialMotion, ReflexionREST, $ionicLoading) {

    $ionicLoading.show();

	ReflexionREST.list({category__slug:$stateParams.slug}, function(data){
		$scope.reflexions = data.results;
		  
        $ionicLoading.hide();

        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
	});
    // Set Ink
    ionicMaterialInk.displayEffect();

})