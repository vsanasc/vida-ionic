angular.module('starter.controllers')

.controller('NotificationCtrl', function($scope, $timeout, $stateParams, ionicMaterialInk, ionicMaterialMotion, CategoryREST, $ionicLoading) {

    $ionicLoading.show();

	CategoryREST.list(function(data){
		$scope.categories = data.results;

        $ionicLoading.hide();

		$timeout(function() {
            ionicMaterialMotion.fadeSlideIn({
                selector: '.animate-fade-slide-in .item'
            });
        }, 200);
	});

      // Set Ink
    ionicMaterialInk.displayEffect();

})