angular.module('starter.controllers')

.controller('TriviaCtrl', function($scope, $rootScope, $timeout, $stateParams, ionicMaterialInk, TriviaREST, VoteREST, $ionicLoading) {


	$scope.loadTrivia = function(){
		$scope.message_wait = $scope.not_found = null;

		$ionicLoading.show();

		TriviaREST.list(function(data){
			var trivias = [];
			for(var i=0;i<data.results.length;i++){

				var range = moment.range(data.results[i].start_to, data.results[i].stop_to)
				
				if(range.contains(moment())){
					var current = data.results[i];
					if(current.vote != ""){
						var vote = current.vote;
					}
				}else{
					if(moment(data.results[i].stop_to).diff(moment(), 'minutes') < 0){
						trivias.push(data.results[i]);
					}
				}
			}

			$scope.trivias = trivias;
			$scope.vote = vote;
			if(current){
				$scope.current = current;
			}else{
				
				if(moment(data.results[0].start_to).diff(moment(), 'minutes') > 0){
					$scope.message_wait = "Espere, la próxima pregunta estará disponible en:";
					$scope.next_trivia = Math.floor((moment(data.results[0].start_to)._d - moment()._d) / 1000);

				}else{
					$scope.not_found = "No hay ninguna pregunta programada";
				}
			}
			$ionicLoading.hide();
		});

	}
	$scope.loadTrivia();
	$scope.sendVote = function(option)
	{
		$ionicLoading.show();

		if(!$scope.vote){

			var vote = new VoteREST()
			vote.user = $rootScope.loginSession.id;
			vote.choose = option.id.toString();
			vote.$save(function(){
				$scope.vote = option.id.toString();

				$ionicLoading.hide();
			});

		}
	}

    ionicMaterialInk.displayEffect();
})