angular.module('starter')

.run(function ($rootScope, $state, $location, $http) {

	if(ionic.Platform.isIOS()){
		var config = {
		    "badge": true,
		    "sound": true,
		    "alert": true,
		    "ecb":'onNotification'
		};
	}
	if(ionic.Platform.isAndroid()){
		var config = {
			"senderID": window.SENDER_ID,
			"ecb":"onNotification"
		}
	}

	document.addEventListener("deviceready", function(){
		plugins.pushNotification.register(function(result){
			
			if(ionic.Platform.isIOS()){
				localStorage.setItem('token_device', result);
			}
		}, function(err){
			console.log(err);
		}, config);


	});

	if(!window.cordova){
		localStorage.setItem('token_device', 'development')	
	}

});

var onNotification = function(e){

	if(ionic.Platform.isAndroid()){
		if(e.regid){
			localStorage.setItem('token_device', e.regid);
		}
	}
	if(ionic.Platform.isIOS()){
	}
	if(e.router){
		location.href = e.router;
	}
}

