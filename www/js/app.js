// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngResource', 'ngCordova','starter.controllers', 'ionic-material', 'timer'])

.run(function($ionicPlatform, $ionicHistory, $cordovaDevice){

	$ionicPlatform.ready(function() {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
		// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
		if(window.cordova){
			window.UUID_DEVICE = $cordovaDevice.getUUID(); 
	  	}else{
			window.UUID_DEVICE = 'development-uuid';	
	  	}

		$ionicPlatform.registerBackButtonAction(function () {
			var history = $ionicHistory.viewHistory();
  			if (history.backView == null) {
  				navigator.notification.confirm(
          			'¿Realmente deseas salir de la aplicación?', 
          			function(button) {
              			if (button == 2) {
                  			navigator.app.exitApp();
              			} 
          			}, 
          			'Salir',
        			'Sí,No'
        		);  
  			} else {
    			$ionicHistory.goBack(-1);
  			}
		}, 100);
	});

})
.config(['$resourceProvider', function($resourceProvider) {
  // Don't strip trailing slashes from calculated URLs
  $resourceProvider.defaults.stripTrailingSlashes = false;

}])
.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider

	.state('app', {
		url: "/app",
		abstract: true,
		templateUrl: "templates/menu.html"
	})

	.state('app.feed', {
		url: "/feed",
		views: {
			'menuContent': {
				templateUrl: "templates/feed.html",
				controller: 'FeedCtrl'
			}
		}
	})

	.state('app.settings', {
		url: "/settings",
		views: {
			'menuContent': {
				templateUrl: "templates/settings.html",
				controller: "SettingsCtrl"
			}
		}
	})

	.state('app.category', {
		url: "/categories",
		views: {
			'menuContent': {
				templateUrl: "templates/category.html",
				controller: 'CategoryCtrl'
			}
		}
	})

	.state('app.reflexions', {
		url: "/categories/:slug",
		cache:false,
		views: {
			'menuContent': {
				templateUrl: "templates/reflexion.html",
				controller: 'ReflexionCtrl'
			}
		}
	})

	.state('app.detail-reflexion', {
		url: "/reflexion/:slug",
		views: {
			'menuContent': {
				templateUrl: "templates/detail-reflexion.html",
				controller: 'DetailReflexionCtrl'
			}
		}
	})

	.state('app.login', {
		url: '/login',
		views: {
			'menuContent': {
					templateUrl: 'templates/login.html',
					controller: 'LoginCtrl'
			}
		}
	})

	.state('app.notifications', {
		url: '/notifications',
		views: {
			'menuContent': {
					templateUrl: 'templates/notifications.html',
					controller: 'NotificationCtrl'
			}
		}
	})

	.state('app.terms', {
		url: '/terms',
		views: {
			'menuContent': {
					templateUrl: 'templates/terms.html',
					controller: 'TermsCtrl'
			}
		}
	})

	.state('app.help', {
		url: '/help',
		views: {
			'menuContent': {
					templateUrl: 'templates/help.html',
					controller: 'HelpCtrl'
			}
		}
	})

	.state('app.alert', {
		url: '/bulletin-board',
		cache:false,
		views: {
			'menuContent': {
					templateUrl: 'templates/alert.html',
					controller: 'AlertCtrl'
			}
		}
	})

	.state('app.trivia', {
		url: '/trivias',
		cache:false,
		views: {
			'menuContent': {
				templateUrl: 'templates/trivia.html',
				controller: 'TriviaCtrl'
			}
		}
	})

	.state('app.information', {
		url: '/information',
		views: {
			'menuContent': {
				templateUrl: 'templates/information.html',
				controller: 'InformationCtrl'
			}
		}
	})

	if(window.isIphone4)
		$ionicConfigProvider.views.transition('none');


	// if none of the above states are matched, use this as the fallback
	if(localStorage.getItem('login') == null){
		$urlRouterProvider.otherwise('/app/login');
	}else{
		$urlRouterProvider.otherwise('/app/feed');
	}

});

angular.module('starter.controllers', ['starter.factories', 'ngCordova']);
angular.module('starter.directives', []);
angular.module('starter.filters', []);
angular.module('starter.factories', ['ngResource'])
