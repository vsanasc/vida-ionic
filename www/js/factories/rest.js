
angular.module('starter.factories')

.factory('CategoryREST', ['$resource', function($resource) {
 	return $resource(window.SERVER + '/api/categories/', null, {
 		'list': { method:'GET', isArray:false},
 	});
}])

.factory('ReflexionREST', ['$resource', function($resource) {
 	return $resource(window.SERVER + '/api/articles/:slug/', {slug:'@slug'}, {
 		'list': { method:'GET', isArray:false},
 	});
}])

.factory('AlertREST', ['$resource', function($resource) {
 	return $resource(window.SERVER + '/api/alerts/', null, {
 		'list': { method:'GET', isArray:false},
 	});
}])

.factory('TriviaREST', ['$resource', function($resource) {
 	return $resource(window.SERVER + '/api/trivias/:id/', {id:'@id'}, {
 		'list': { method:'GET', isArray:false},
 	});
}])

.factory('VoteREST', ['$resource', function($resource) {
 	return $resource(window.SERVER + '/api/votes/:id/', {id:'@id'});
}])

.factory('FeedREST', ['$resource', function($resource) {
 	return $resource(window.SERVER + '/api/feed/:id/', {id:'@id'}, {
 		'list': { method:'GET', isArray:false},
 	});
}])