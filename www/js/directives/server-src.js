angular.module('starter.directives')

.directive('serverSrc', function(){

    return{
        link: function postLink(scope, element, attrs) {
            attrs.$observe('serverSrc', function(newVal, oldVal){
                if(newVal != undefined && newVal != '' && newVal != null){

                    var img = new Image();
                    var src = newVal.indexOf('http') === 0 ? attrs.serverSrc : (window.SERVER+attrs.serverSrc);
                    
                    img.src = src;
                    element.attr('src', attrs.imageDefault);
                    angular.element(img).bind('load', function () {
                        element.attr("src", src); 
					});

                }else{
                    element.attr('src', attrs.imageDefault);
                }
            });
 
        }
    }

});